function oneThroughTwenty() {
    
  /* Your code goes below
  Write a for or a while loop
  console.log() the result*/
   
  for (var i = 1; i <= 20; i++){
     console.log(i)
  }
}

oneThroughTwenty() //call function oneThroughTwenty

function evensToTwenty() {
   
  /* Your code goes below
  Write a for or a while loop
  console.log() the result */
  
  for (var i = 1; i <= 20; i++){
    if (i%2 === 0){
   console.log(i)
 }
}
}

evensToTwenty() //call function evensToTwenty

function oddsToTwenty() {
   
 /* Your code goes below
 Write a for or a while loop
 console.log() the result */
 
 for (var i = 1; i <= 20; i++){
   if (i%2 === 1){
  console.log(i)
 }
}
}


oddsToTwenty() //call function oddsToTwenty

function multiplesOfFive() {
   
 /* Your code goes below
 Write a for or a while loop
 console.log() the result */

 for (var i = 1; i <= 100; i++){
   if (i%5 === 0){
  console.log(i)
 }
}
}

multiplesOfFive() //call function multiplesOfFive

function squareNumbers() {
   
/* Your code goes below
 Write a for or a while loop
 console.log() the result */

 for (var i = 1; i <= 100; i++){
   let quad = parseInt(Math.sqrt(i))
    if ((quad*quad) === i){
     console.log(i)
 }
}
}

squareNumbers() //call function squareNumbers

function countingBackwards() {
   
 /* Your code goes below
 Write a for or a while loop
 console.log() the result */
 for (var i = 20; i >= 1; i--){
   console.log(i)
}
}

countingBackwards() //call function countingBackwards

function evenNumbersBackwards() {
   
/* Your code goes below
 Write a for or a while loop
 console.log() the result */
 for (var i = 20; i >= 1; i--){
   if (i%2 === 0){
  console.log(i)
 }
}
}

evenNumbersBackwards() //call function evenNumbersBackwards

function oddNumbersBackwards() {
   
/* Your code goes below
 Write a for or a while loop
 console.log() the result */
 for (var i = 20; i >= 1; i--){
   if (i%2 === 1){
  console.log(i)
 }
}
}

oddNumbersBackwards() //call function oddNumbersBackwards

function multiplesOfFiveBackwards() {
   
/* Your code goes below
 Write a for or a while loop
 console.log() the result */
 for (var i = 100; i >= 1; i--){
   if (i%5 === 0){
  console.log(i)
 }
}
}

multiplesOfFiveBackwards() //call function multiplesOfFiveBackwards

function squareNumbersBackwards() {
   
  /* Your code goes below
 Write a for or a while loop
 console.log() the result */
 for (var i = 100; i >= 1; i--){
   let quad = parseInt(Math.sqrt(i))
    if ((quad*quad) === i){
     console.log(i)
 }
}
}

squareNumbersBackwards() //call function squareNumbersBackwards
