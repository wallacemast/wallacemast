// comece a criar a sua função add na linha abaixo
function add(arg1, arg2){ return arg1+arg2}

// descomente a linha seguinte para testar sua função
console.assert(add(3, 5) === 8, 'A função add não está funcionando como esperado');

// comece a criar a sua função multiply na linha abaixo
function multiply(arg1, arg2){return arg1*arg2}

// descomente a linha seguinte para testar sua função
 console.assert(multiply(4, 6) === 24, 'A função multiply não está funcionando como esperado');


// comece a criar a sua função power na linha abaixo
function power(arg1, arg2){return arg1**arg2 }

// descomente a linha seguinte para testar sua função
console.assert(power(3, 4) === 81, 'A função power não está funcionando como esperado');


// comece a criar a sua função factorial na linha abaixo
function factorial(arg){
    total = arg
    for (let i = arg-1;i>0;i--){
         total = multiply(total,i)
 }
 return total
}
// descomente a linha seguinte para testar sua função
console.assert(factorial(5) === 120, 'A função factorial não está funcionando como esperado');


/**
 * BONUS (aviso: o grau de dificuldade é bem maior !!!)
 */

// crie a função fibonacci
function fibonacci(numero){
    let pen = 0 
    let ult = 1
    let atual
    if (numero<=2){
        return numero -1
    } else {
        for (i=3; i<=numero;i++){
            atual = add(pen,ult)
            pen = ult
            ult = atual
        }
    }
return ult
}

// descomente a linha seguinte para testar sua função
console.assert(fibonacci(8) === 13, 'A função fibonacci não está funcionando como esperado');
